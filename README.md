# soal-shift-sisop-modul-4-E08-2022

Anggota :
> Januar Evan - 5025201210\
> Graidy Megananda - 5025201188\
> Helmi Taqiyudin - 5025201152

---
## Tabel Konten
- [Soal 1](#nomor-1)
- [Soal 2](#nomor-2)
- [Kesulitan](#kesulitan)

## Nomor 1

FUSE functions yang dipakai dalam soal ini adalah `.getattr()`, `.readdir()`, `.read()`, `.write()`, `.rename()`, `.open()`
1.  getattr()
    ```c
    static int xmp_getattr(const char* path, struct stat* stbuf){
    	char *strenc = strstr(path, prefix);
    	// printf("%s -- ", fixedpath);
    	if(strenc != NULL){
    		decode1(strenc);
    	}
    	// printf("%s\n", path);
    	char pathnew[FILENAME_MAX];
    	
    	if(strcmp(path,"/") == 0){
    		path = directoryPath;
    		sprintf(pathnew, "%s", path);
    	}
    	else{
    		sprintf(pathnew, "%s%s", directoryPath, path);
    	}
    	// printf("\t %s\n", pathnew);
    	int res = 0;
    	printf("LSTAT %s\n", pathnew);
    	res = lstat(pathnew, stbuf);
    	if(res == -1){
    		return -errno;
    	}
    	else{
    		return 0;
    	}
    }
    ```
    getattr() ini digunakan untuk mengambil atribut dari setiap file atau directory
    
2.  readdir()
    ```c
    static int xmp_readdir(const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi){
    	char *strenc = strstr(path, prefix);
    	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
    
    	char pathnew[FILENAME_MAX];
    	
    	if(strcmp(path,"/") == 0){
    		path = directoryPath;
    		sprintf(pathnew, "%s", path);
    	}
    	else{
    		sprintf(pathnew, "%s%s", directoryPath, path);
    	}
    	int res = 0;
    	struct dirent *dir;
    	DIR *d;
    	(void) fi;
    	(void) offset;
    	d = opendir(pathnew);
    	if(d == NULL){
    		return -errno;
    	}
    	while ((dir = readdir(d)) != NULL){
    		struct stat st;
    		memset(&st, 0, sizeof(st));
    		st.st_ino = dir->d_ino;
    		st.st_mode = dir->d_type << 12;
    		if(strenc != NULL){
    			encode1(dir->d_name);
    			printf("READDIR :: %s\n", pathnew);
    		}
    		res = (filler(buf, dir->d_name, &st, 0));
    		if(res != 0){
    			break;
    		}
    	}
    	closedir(d);
    	return 0;
    }
    ```
    readdir() ini digunakan untuk membaca tempat directory

3.  read()
    ```c
    static int xmp_read(const char* path, char *buf, size_t size, off_t offset, struct fuse_file_info* fi){
    	char *strenc = strstr(path, prefix);
    	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
    	char pathnew[FILENAME_MAX];
    	sprintf(pathnew, "%s%s", directoryPath, path);
    	printf("yak ini read %s\n", pathnew);
    	int fd;
    	int res; 	
    	(void) fi;
    	fd = open(pathnew, O_RDONLY);
    	if(fd == -1){
    		return -errno;
    	}
    	res = pread(fd, buf, size, offset);
    	printf("READ :: %s\n", pathnew);
    	if(res == -1){
    		return -errno;
    	}
    	close(fd);
    	return res;
    }
    ```
    read() ini digunakan untuk membaca nilai dari sebuah file

4.  write()
    ```c
    static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    	char *strenc = strstr(path, prefix);
    	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
    	char pathnew[FILENAME_MAX];
    	sprintf(pathnew, "%s%s", directoryPath, path);
    	printf("yak ini write %s\n", pathnew);
    	int fd;
    	int res; 	
    	(void) fi;
    	fd = open(pathnew, O_WRONLY);
    	if(fd == -1){
    		return -errno;
    	}
    	res = pwrite(fd, buf, size, offset);
    	printf("WRITE :: %s\n", pathnew);
    	if(res == -1){
    		return -errno;
    	}
    	close(fd);
    	return res;
    }
    ```
    write() digunakan apabila ingin menulis suatu file
    
5.  rename()
    ```c
    static int xmp_rename(const char* from, const char* to){
    	char fsource[128], fdest[128];
    	sprintf(fsource, "%s%s", directoryPath, from);
    	sprintf(fdest, "%s%s", directoryPath, to);
    
    	char str[FILENAME_MAX];
    	char str2[FILENAME_MAX];
    	char *folderpath = strstr(fsource, prefix);
    	if(folderpath != NULL){
    		strcpy(str2, "RENAME terdecode ");
    	}
    	else{
    		strcpy(str2, "RENAME terenkripsi ");
    	}
    	strcpy(str, str2);
    	strcat(str, fsource);
    	logging2(str, fdest);
    	int res;
    	res = rename(fsource, fdest);
    	printf("RENAME :: %s to %s\n", fsource, fdest);
    	if(res == -1){
    		return -errno;
    	}
    	else{
    		return 0;
    	}
    }
    ```
    rename() digunakan untuk merename suatu file atau directory

6.  open()
    ```c
    static int xmp_open(const char *path, struct fuse_file_info *fi)
    {
    	char *strenc = strstr(path, prefix);
    	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
    	char pathnew[FILENAME_MAX];
    	sprintf(pathnew, "%s%s", directoryPath, path);
    	printf("yak ini open %s\n", pathnew);
    	int res;
    
    	res = open(pathnew, fi->flags);
    	if (res == -1)
    		return -errno;
    
    	fi->fh = res;
    	return 0;
    }
    ```
    open() digunakan untuk membuka file dari sebuah directory

>Encode dan Decoder

1.  Encode
    ```c
    void encode1(char* strEnc1) { 
    	if(strcmp(strEnc1, ".") == 0 || strcmp(strEnc1, "..") == 0)
            return;
        
        	int strLength = strlen(strEnc1);
        for(int i = 0; i < strLength; i++) {
    		if(strEnc1[i] == '/') 
                		continue;
    		if(strEnc1[i]=='.')
    	            break;
            
    		if(strEnc1[i]>='A'&&strEnc1[i]<='Z'){
    	        strEnc1[i] = 'Z'+'A'-strEnc1[i];
            }
            if(strEnc1[i]>='a'&&strEnc1[i]<='z'){
                if(strEnc1[i] > 109){
                    strEnc1[i] -= 13;
                }
                else{
                    strEnc1[i] += 13;
                }
            }
    	}
    }
    ```
    ini untuk encode dengan KAPITAL menggunakan `atbash cipher` dan huruf kecil menggunakan `rot13`
    
2.  Decode
    ```c
    void decode1(char * strDec1){ 
    	if(strcmp(strDec1, ".") == 0 || strcmp(strDec1, "..") == 0 || strstr(strDec1, "/") == NULL) 
            return;
        
        	int strLength = strlen(strDec1), s=0;
    	for(int i = strLength; i >= 0; i--){
    		if(strDec1[i]=='/')break;
    
    		if(strDec1[i]=='.'){//nyari titik terakhir
    			strLength = i;
    			break;
    		}
    	}
    	for(int i = 0; i < strLength; i++){
    		if(strDec1[i]== '/'){
    			s = i+1;
    			break;
    		}
    	}
       	for(int i = s; i < strLength; i++) {
    		if(strDec1[i] =='/'){
       	         continue;
       	     }
       	     if(strDec1[i]>='A'&&strDec1[i]<='Z'){
       	         strDec1[i] = 'Z'+'A'-strDec1[i];
       	     }
       	     if(strDec1[i]>='a'&&strDec1[i]<='z'){
       	        if(strDec1[i] > 109){
                    strDec1[i] -= 13;
                }
                else{
                    strDec1[i] += 13;
                }
       	     }
       	 }	
    }
    ```
    Decoder dari `atbash cipher` dan `rot13`

>   Logging

1. logging
    ```c
    void logging2(const char* old, char* new) {
    	FILE * logFile;
    	logFile = fopen("/home/januarevan/Wibu.log", "a");
    	fprintf(logFile, "%s -> %s\n", old, new);
        fclose(logFile);
    }
    ```
    logging dipanggil pada saat rename dari Animeku_... menjadi ... dan sebaliknya
    
## Nomor 2

FUSE functions yang dipakai dalam soal ini adalah `.getattr()`, `.readdir()`, `.read()`, `.write()`, `.rename()`, `.open()`, `.truncate()`, `.mkdir()`, `.mknod()`, `.unlink()`, `.rmdir()`
1.  getattr()
    ```c
    static int xmp_getattr(const char* path, struct stat* stbuf){
    	char *strenc = strstr(path, prefix);
    	// printf("%s -- ", fixedpath);
    	if(strenc != NULL){
    		decode1(strenc);
    	}
        else{
            strenc = strstr(path, "IAN_");
            if(strenc != NULL){
                decode(strenc, "INNUGANTENG");
            }
        }
        
    	// printf("%s\n", path);
    	char pathnew[FILENAME_MAX];
    	
    	if(strcmp(path,"/") == 0){
    		path = directoryPath;
    		sprintf(pathnew, "%s", path);
    	}
    	else{
    		sprintf(pathnew, "%s%s", directoryPath, path);
    	}
    	// printf("\t %s\n", pathnew);
    	int res = 0;
    	// printf("LSTAT %s\n", pathnew);
    	res = lstat(pathnew, stbuf);
    	if(res == -1){
    		return -errno;
    	}
    	else{
    		return 0;
    	}
    }
    ```
    getattr() ini digunakan untuk mengambil atribut dari setiap file atau directory
    
2.  readdir()
    ```c
    static int xmp_readdir(const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi){
    	char *strenc = strstr(path, prefix);
    	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
        else{
            strenc = strstr(path, "IAN_");
            if(strenc != NULL){
                decode(strenc, "INNUGANTENG");
            }
        }
    
    	char pathnew[FILENAME_MAX];
    	
    	if(strcmp(path,"/") == 0){
    		path = directoryPath;
    		sprintf(pathnew, "%s", path);
    	}
    	else{
    		sprintf(pathnew, "%s%s", directoryPath, path);
    	}
    	int res = 0;
    	struct dirent *dir;
    	DIR *d;
    	(void) fi;
    	(void) offset;
    	d = opendir(pathnew);
    	if(d == NULL){
    		return -errno;
    	}
    	while ((dir = readdir(d)) != NULL){
    		struct stat st;
    		memset(&st, 0, sizeof(st));
    		st.st_ino = dir->d_ino;
    		st.st_mode = dir->d_type << 12;
            // printf("READDIR1 :: %s\n", strenc);
            strenc = strstr(path, prefix);
            if(strenc != NULL){
                encode1(dir->d_name);
            }
            else{
                strenc = strstr(path, "IAN_");
                if(strenc != NULL){
                    encode(dir->d_name, "INNUGANTENG");
                }
            }
    		// printf("READDIR2 :: %s\n", pathnew);
    		res = (filler(buf, dir->d_name, &st, 0));
    		if(res != 0){
    			break;
    		}
    	}
    	closedir(d);
    	return 0;
    }
    ```
    readdir() ini digunakan untuk membaca tempat directory

3.  read()
    ```c
    static int xmp_read(const char* path, char *buf, size_t size, off_t offset, struct fuse_file_info* fi){
    	char *strenc = strstr(path, prefix);
	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
        else{
            strenc = strstr(path, "IAN_");
            if(strenc != NULL){
                decode(strenc, "INNUGANTENG");
            }
        }
    
    	char pathnew[FILENAME_MAX];
    	sprintf(pathnew, "%s%s", directoryPath, path);
    	printf("yak ini read %s\n", pathnew);
    	int fd;
    	int res; 	
    	(void) fi;
    	fd = open(pathnew, O_RDONLY);
    	if(fd == -1){
    		return -errno;
    	}
    	res = pread(fd, buf, size, offset);
    	printf("READ :: %s\n", pathnew);
    	if(res == -1){
    		return -errno;
    	}
    	close(fd);
    	return res;
    }
    ```
    read() ini digunakan untuk membaca nilai dari sebuah file

4.  write()
    ```c
    static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    	char *strenc = strstr(path, prefix);
    	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
        else{
            strenc = strstr(path, "IAN_");
            if(strenc != NULL){
                decode(strenc, "INNUGANTENG");
            }
        }
    
    	char pathnew[FILENAME_MAX];
    	sprintf(pathnew, "%s%s", directoryPath, path);
    	printf("yak ini write %s\n", pathnew);
    	int fd;
    	int res; 	
    	(void) fi;
    	fd = open(pathnew, O_WRONLY);
    	if(fd == -1){
    		return -errno;
    	}
        char str[100];
    	sprintf(str, "WRITE::%s", path);
    	logging(str,1);
    	res = pwrite(fd, buf, size, offset);
    	printf("WRITE :: %s\n", pathnew);
    	if(res == -1){
    		return -errno;
    	}
    	close(fd);
    	return res;
    }
    ```
    write() digunakan apabila ingin menulis suatu file
    
5.  rename()
    ```c
    static int xmp_rename(const char* from, const char* to){
    	char fsource[128], fdest[128];
    	sprintf(fsource, "%s%s", directoryPath, from);
    	sprintf(fdest, "%s%s", directoryPath, to);
    
    	char str[FILENAME_MAX];
    	char str2[FILENAME_MAX];
    	char *sourcepath1 = strstr(fsource, prefix);
        char *destpath1 = strstr(fdest, prefix);
    	if(sourcepath1 != NULL && destpath1 == NULL){
    		strcpy(str2, "RENAME terdecode ");
    		strcpy(str, str2);
    		strcat(str, fsource);
    		logging2(str, fdest);
    	}
    	else if(sourcepath1 == NULL && destpath1 != NULL){
    		strcpy(str2, "RENAME terenkripsi ");
    		strcpy(str, str2);
    		strcat(str, fsource);
    		logging2(str, fdest);
    	}
        else{
            sourcepath1 = strstr(fsource, "IAN_");
            destpath1 = strstr(fdest, "IAN_");
            char str[100];
            sprintf(str, "RENAME::%s::%s", from, to);
            logging(str,1);
        }
    	int res;
    	res = rename(fsource, fdest);
    	printf("RENAME :: %s to %s\n", fsource, fdest);
    	if(res == -1){
    		return -errno;
    	}
    	else{
    		return 0;
    	}
    }
    ```
    rename() digunakan untuk merename suatu file atau directory

6.  open()
    ```c
    static int xmp_open(const char *path, struct fuse_file_info *fi)
    {
    	char *strenc = strstr(path, prefix);
    	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
        else{
            strenc = strstr(path, "IAN_");
            if(strenc != NULL){
                decode(strenc, "INNUGANTENG");
            }
        }
    
    	char pathnew[FILENAME_MAX];
    	sprintf(pathnew, "%s%s", directoryPath, path);
    	printf("yak ini open %s\n", pathnew);
    	int res;
    
    	res = open(pathnew, fi->flags);
    	if (res == -1)
    		return -errno;
    
    	fi->fh = res;
    	return 0;
    }
    ```
    open() digunakan untuk membuka file dari sebuah directory

7.  truncate()
    ```c
    static int xmp_truncate(const char *path, off_t size)
    {
    	char *strenc = strstr(path, prefix);
    	
    	if(strenc != NULL){
    		decode1(strenc);
    	}
        else{
            strenc = strstr(path, "IAN_");
            if(strenc != NULL){
                decode(strenc, "INNUGANTENG");
            }
        }
    
    	char pathnew[FILENAME_MAX];
    	sprintf(pathnew, "%s%s", directoryPath, path);
    	int res;
        res = truncate(pathnew, size);
        printf("TRUNCATE :: %s\n", pathnew);
    	if (res == -1)
    		return -errno;
    
    	return 0;
    }
    ```
    truncate untuk menambah isi dari file

8.  mkdir()
    ```c
    static int xmp_mkdir(const char *path, mode_t mode){ 
    	char newPath[1000];
    	if(strcmp(path,"/") == 0){
    		path=directoryPath;
    		sprintf(newPath,"%s",path);
    	}
    	else 
            sprintf(newPath, "%s%s",directoryPath,path);
    
    	int result = mkdir(newPath, mode);
        char str[100];
    	sprintf(str, "MKDIR::%s", path);
    	logging(str,1);
    
    	char * folderPath = strstr(path, prefix);
        
    	if(folderPath != NULL) {
            	logging2(newPath, newPath);
        	}
    	
    	printf("%s\n",path);
    	printf("%s\n",newPath);
    	if (result == -1)
    		return -errno;
    
    	return 0;
    }
    ```
    mkdir digunakan untuk membuat directory baru

9.  mknod()
    ```c
    static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){
    	char newPath[1000];
    	if(strcmp(path,"/") == 0){
    		path = directoryPath;
    		sprintf(newPath,"%s",path);
    	} 
        	else 
            	sprintf(newPath, "%s%s",directoryPath,path);
    	
        	int result;
    
    	if (S_ISREG(mode)) {
    		result = open(newPath, O_CREAT | O_EXCL | O_WRONLY, mode);
    		if (result >= 0)
    			result = close(result);
    	} else if (S_ISFIFO(mode))
    		result = mkfifo(newPath, mode);
    	else
    		result = mknod(newPath, mode, rdev);
        	
        	char str[100];
    	sprintf(str, "MKNOD::%s", path);
    	logging(str,1);
    	if (result == -1)
    		return -errno;
    
    	return 0;
    }
    ```
10.  unlink()
    ```c
    static int xmp_unlink(const char *path) { 
    	char * strToEnc1 = strstr(path, prefix);
    	if(strToEnc1 != NULL){
            	decode1(strToEnc1); 
        	}
    
    	char newPath[1000];
    	if(strcmp(path,"/") == 0){
    		path=directoryPath;
    		sprintf(newPath,"%s",path);
    	} 
        	else 
            	sprintf(newPath, "%s%s",directoryPath,path);
        	char str[100];
    	sprintf(str, "UNLINK::%s", path);
    	
    	char * folderPath = strstr(path, prefix);
        
    	if(folderPath != NULL) {
            	logging(str,2);
        	}
    
    	int result;
    	result = unlink(newPath);
    	if (result == -1)
    		return -errno;
    
    	return 0;
    }
    ```
    unlink digunakan ketika menghapus file
11.  rmdir()
    ```c
    static int xmp_rmdir(const char *path) {
    	char * strToEnc1 = strstr(path, prefix);
    	if(strToEnc1 != NULL){
            	decode1(strToEnc1); 
        	}
    	printf("RMDIR :: %s\n", path);
    
    	char newPath[1000];
    	sprintf(newPath, "%s%s",directoryPath,path);
        	char str[100];
    	sprintf(str, "RMDIR::%s", path);
    	logging(str,2);
    	int result;
    	result = rmdir(newPath);
    	if (result == -1)
    		return -errno;
    
    	return 0;
    }
    ```


>Encode dan Decoder

1.  Encode
    ```c
    void encode(char * strEnc1, char * key) {
        printf("ENCODE.0 :: %s\n", strEnc1);
        if(strcmp(strEnc1, ".") == 0 || strcmp(strEnc1, "..") == 0)
            return;
        
        int strLength = strlen(strEnc1), keyLen = strlen(key);
        int i, j;
        char newKey[strLength];
        for(i = 0, j = 0; i < strLength; ++i, ++j){
            if(j == keyLen)
                j = 0;
            newKey[i] = key[j];
        }
        for(int i = 0; i < strLength; i++) {
    		if(strEnc1[i] == '/') 
                		continue;
    		if(strEnc1[i]=='.')
    	            break;
            
    		strEnc1[i] = ((strEnc1[i] + newKey[i]) % 26) + 'A';
    	}
        printf("ENCODE.1 :: %s\n", strEnc1);
    }
    ```
    ini untuk encode dengan `vigenere cipher` dengan key `INNUGANTENG`
    
2.  Decode
    ```c
    void decode(char * strDec1, char * key) {
        printf("DECODE.0 :: %s\n", strDec1);
        if(strcmp(strDec1, ".") == 0 || strcmp(strDec1, "..") == 0 || strstr(strDec1, "/") == NULL) 
            return;
        
        int strLength = strlen(strDec1), s=0;
        int msgLen = strlen(strDec1), keyLen = strlen(key), i, j;
        char newKey[msgLen];
        for(i = 0, j = 0; i < msgLen; ++i, ++j){
            if(j == keyLen)
                j = 0;
     
            newKey[i] = key[j];
        }
    	for(int i = strLength; i >= 0; i--){
    		if(strDec1[i]=='/')break;
    
    		if(strDec1[i]=='.'){//nyari titik terakhir
    			strLength = i;
    			break;
    		}
    	}
    	for(int i = 0; i < strLength; i++){
    		if(strDec1[i]== '/'){
    			s = i+1;
                j = 0;
    			break;
    		}
    	}
       	for(int i = s; i < strLength; i++, j++) {
    		if(strDec1[i] =='/'){
                j = 0;
       	        continue;
       	    }
            strDec1[i] = (((strDec1[i] - newKey[j]) + 26) % 26) + 'A';
            
       	}
        printf("DECODE.1 :: %s\n", strDec1); 
    }

    ```
    Decoder dari `vigenere cipher` dengan key `INNUGANTENG`

>   Logging

1. logging
    ```c
    void logging(char* c, int type){
        printf("LOGGING :: %s, %d\n", c, type);
    	time_t currTime;
    	struct tm * timeinfo;
    	time ( &currTime );
    	timeinfo = localtime (&currTime);
    	
    	FILE * logFile;
    	logFile = fopen("/home/januarevan/hayolongapain_e08.log", "a");
    		
        if(type==1){//info
            printf("INFO LOGGING :: %s\n", c);
            fprintf(logFile, "INFO::%d%d%d-%d:%d:%d::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, c);
        }
        else if(type==2){ //warning
            printf("WARNING LOGGING :: %s\n", c);
            fprintf(logFile, "WARNING::%d%d%d-%d:%d:%d::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, c);
        }
        fclose(logFile);
    }
    ```
    logging dipanggil pada saat rename dari Animeku_... menjadi ... dan sebaliknya
## Kesulitan
Kesulitan dalam pengerjaan nomor 3 adalah karena tidak bisa melakukan debug menggunakan printf (untuk mengecek apakah fungsi yang dibuat untuk menyelesaikan nomor tidak terpanggil atau tidak). Rincian:
- Lokasi file c dan folder yang akan di mount ada di `Documents`. Lokasi target fuse adalah `downloads`.
- Pertama melakukan compile seperti yang tertulis pada modul
- Menjalankan code dengan `./anya_E08 -f (nama_folder_di_documents)`
- Program tidak dapat berjalan dengan baik. Setelah program selesai, seluruh file yang berada pada folder `Documents` akan dihapus dan memunculkan file baru bernama `.0`
Kami tidak menemukan cara untuk menampilkan `printf` untuk membantu mendebug selain menggunakan `-f`. Namun, karena terjadi error seperti yang dijelaskan diatas, akhirnya kami kesulitan untuk mendebugnya.
